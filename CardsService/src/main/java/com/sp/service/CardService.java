package com.sp.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.sp.model.Card;
import com.sp.repository.CardRepository;

@Service
public class CardService {

	@Autowired
	CardRepository cRepository;

	public int addCard(Card c) {
		try {
			Card cardSaved = cRepository.save(c);
			return cardSaved.getId();
		} catch(Exception e) {
			System.out.println("Erreur d'ajout d'une carte");
			return -1;
		}
	}

	public Card getCard(int id) {
		try {
			Card card = cRepository.findById(id);
			if (card == null) {
				System.out.println("Pas de carte trouvée pour l'id " + id);
			}
			return card;
		} catch(Exception e) {
			System.out.println("Erreur d'acces à la BD");
		}
		return null;
	}

	public List<Card> getAllCards() {
		try {
			return cRepository.findAll();
		} catch(Exception e) {
			System.out.println("Erreur de récupération de toutes les cartes");
		}
		return null;
	}
	
	public List<Card> getAllCardstoSell() {
		try {
			List<Card> listCardsToSell = cRepository.findByUserId(-1);
			return listCardsToSell;
		} catch(Exception e) {
			System.out.println("Erreur de récupération des cartes à vendre");
		}
		return null;
	}

	public List<Card> getUserCards(int user_id) {
		try {
			List<Card> listUserCards = cRepository.findByUserId(user_id);
			return listUserCards;
		} catch(Exception e) {
			System.out.println("Erreur de récupération des cartes d'un user");
		}
		return null;
	}

	public int getPriceCard(Integer id) {
		try {
			return getCard(id).getPrice();
		} catch(Exception e) {
			System.out.println("Erreur de récupération prix carte");
		}
		return -1;
	}

	public boolean removeCardFromUser(int card_id) {
		try {
			Card cardToRemove = getCard(card_id);
			cardToRemove.setUserId(-1);
			try {
				cRepository.save(cardToRemove);
				return true;
			} catch(Exception e) {
				System.out.println("Erreur de sauvegarde de la carte");
			}
			
		} catch(Exception e) {
			System.out.println("Erreur récupération carte");
		}
		return false;
	}

	public boolean setUserToCard(Integer user_id, Integer card_id) {
		try {
			Card card = getCard(card_id);
			card.setUserId(user_id);
			try {
				cRepository.save(card);
				return true;
			} catch(Exception e) {
				System.out.println("Erreur de sauvegarde de la carte");
			}
		} catch(Exception e) {
			System.out.println("Erreur de récupération de la carte");
		}
		return false;
	}
}
