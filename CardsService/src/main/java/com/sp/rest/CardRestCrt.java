package com.sp.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.sp.model.Card;
import com.sp.service.CardService;

import java.util.List;
import java.util.Map;

@RestController
public class CardRestCrt {
    @Autowired
    CardService cService;

    @RequestMapping(method=RequestMethod.POST,value="/card/add")
    public int addCard(@RequestBody Card card) {
    	System.out.println();
    	System.out.println("----Ajout de carte----");
    	System.out.println(card);
    	int cardId = cService.addCard(card);
    	System.out.println("Réponse:");
    	System.out.println("cardId: " + cardId);
        return cardId;
    }
    
    @RequestMapping(method=RequestMethod.GET,value="/card/{id}")
    public Card getCard(@PathVariable String id) {
    	System.out.println();
    	System.out.println("----Récupération carte----");
    	System.out.println("id");
    	Card card = cService.getCard(Integer.valueOf(id));
    	System.out.println("Réponse:");
    	System.out.println(card);
        return card;
    }

    @RequestMapping(method=RequestMethod.GET,value="/card/all")
    public List<Card> getAllCards(){
    	System.out.println();
    	System.out.println("----Récupération toutes les cartes");
    	List<Card> cards = cService.getAllCards();
    	System.out.println("Réponse:");
    	System.out.println("Taille liste de carte: " + cards.size());
        return cards;
    }
    
    @RequestMapping(method=RequestMethod.GET,value="/card/tosell")
    public List<Card> getAllCardsToSell(){
    	System.out.println();
    	System.out.println("----Récupération cartes à vendre");
    	List<Card> cards = cService.getAllCardstoSell();
    	System.out.println("Réponse:");
    	System.out.println("Taille liste de carte: " + cards.size());
        return cards;
    }
    
    @RequestMapping(method=RequestMethod.GET,value="/card/userCards/{id}")
    public List<Card> getUserCards(@PathVariable String id){
    	System.out.println();
    	System.out.println("----Récupération carte utilisateur----");
    	System.out.println(id);
    	List<Card> cards = cService.getUserCards(Integer.valueOf(id));
    	System.out.println("Réponse:");
    	System.out.println("Taille liste de carte: " + cards.size());
    	return cards;
    }
    
    @RequestMapping(method=RequestMethod.GET,value="/card/price/{id}")
    public int getPriceCard(@PathVariable String id) {
    	System.out.println();
    	System.out.println("----Prix carte----");
    	System.out.println(id);
    	int price = cService.getPriceCard(Integer.valueOf(id));
    	System.out.println("Réponse:");
    	System.out.println("price: " + price);
        return price;
    }
    
    @RequestMapping(method=RequestMethod.POST, value="/card/removeUser")
	private boolean removeUserFromCard(@RequestBody int cardId) {
    	System.out.println();
    	System.out.println("----Supression d'un user----");
    	System.out.println(cardId);
		boolean success = cService.removeCardFromUser(cardId);
		System.out.println("Réponse:");
		System.out.println("success: " + success);
		return success;
	}
    
    @RequestMapping(method=RequestMethod.POST, value="/card/setUser")
	private boolean setUserToCard(@RequestBody Map<String, Integer> param) {
    	System.out.println();
    	System.out.println("----Ajout d'un user----");
    	System.out.println(param);
		boolean success = cService.setUserToCard(Integer.valueOf(param.get("user_id")),Integer.valueOf(param.get("card_id")));
		System.out.println("Réponse:");
		System.out.println("Success: " + success);
		return success;
	}
}
