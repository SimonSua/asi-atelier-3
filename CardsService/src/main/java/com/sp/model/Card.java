package com.sp.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Card {
	@Id
	@GeneratedValue
	private int id;
	private String name;
	private String description;
	private String family;
	private String affinity;
	private int hp;
	private int energy;
	private int price;
	private int defence;
	private int attack;
	private String imgUrl;
	private int userId;

	public Card(String name, String description, String family, String affinity, int hp, int energy, int attack, int defence, int price,
			String imgUrl, int userId) {
		super();
		this.name = name;
		this.description = description;
		this.family = family;
		this.affinity = affinity;
		this.hp = hp;
		this.energy = energy;
		this.price = price;
		this.attack = attack;
		this.defence = defence;
		this.imgUrl = imgUrl;
		this.userId = userId;
	}
	
	public Card() {
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(Object userId) {
		this.userId = (int) userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFamily() {
		return family;
	}

	public void setFamily(String family) {
		this.family = family;
	}

	public String getAffinity() {
		return affinity;
	}

	public void setAffinity(String affinity) {
		this.affinity = affinity;
	}

	public int getHp() {
		return hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}

	public int getEnergy() {
		return energy;
	}

	public void setEnergy(int energy) {
		this.energy = energy;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getAttack() {
		return attack;
	}

	public void setAttack(int attack) {
		this.attack = attack;
	}

	public int getDefence() {
		return defence;
	}

	public void setDefence(int defence) {
		this.defence = defence;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	

	@Override
	public String toString() {
		return "Card ["+this.id+"]: name:"+this.name+" imgUrl:"+this.imgUrl;
	}

	
	

}
