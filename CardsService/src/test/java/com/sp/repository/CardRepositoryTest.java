package com.sp.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.sp.model.Card;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CardRepositoryTest {

	@Autowired
	private CardRepository cRepository;
	
	@Before
    public void setUp() {
		Card card = new Card("superman","powerfull","Superman Family",
				"Superman affinity",50,100,550,30,800,"https:\\\\\\\\fakeSite\\\\data.jpg",-1);
		card.setId(1);
		cRepository.save(card);
		card.setId(2);
		cRepository.save(card);
		card.setId(3);
		card.setUserId(1);
		cRepository.save(card);
    }

    @After
    public void cleanUp() {
    	cRepository.deleteAll();
    }
    
    @Test
    public void saveTest() {
    	cRepository.save(new Card("superman","powerfull","Superman Family",
				"Superman affinity",50,100,550,30,800,"https:\\\\\\\\fakeSite\\\\data.jpg",-1));
    	assertTrue(true);
    }
    
    @Test
    public void findByNameTest() {
    	List<Card> cards = cRepository.findByName("superman");
    	assertEquals(cards.size(),3);
    }
    
    @Test
    public void findAllTest() {
    	List<Card> cards = cRepository.findAll();
    	assertEquals(cards.size(),3);
    }
    
    @Test
    public void findByUserIdTest() {
    	List<Card> cards = cRepository.findByUserId(1);
    	assertEquals(cards.size(),1);
    }
    
}
