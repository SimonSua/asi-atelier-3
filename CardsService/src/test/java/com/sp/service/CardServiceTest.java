package com.sp.service;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import com.sp.model.Card;
import com.sp.repository.CardRepository;

@RunWith(SpringRunner.class)
@WebMvcTest(value = CardService.class)
public class CardServiceTest {

	@MockBean
    private RestTemplate restTemplate;

	
	@MockBean
	private CardRepository cardRepository;

	@Autowired
    private CardService cardService;

	
	@Test
	public void addCardSuccess() {
		Card newcard = new Card("superman","powerfull","Superman Family",
				"Superman affinity",50,100,550,30,800,"https:\\\\\\\\fakeSite\\\\data.jpg",-1);
		newcard.setId(1);
		Mockito.when(cardRepository.save(Mockito.any(Card.class))).thenReturn(newcard);
		int result = cardService.addCard(newcard);
		assertEquals(result,1);
	}
	
	@Test
	public void addCardFail() {
		Card newcard = new Card("superman","powerfull","Superman Family",
				"Superman affinity",50,100,550,30,800,"https:\\\\\\\\fakeSite\\\\data.jpg",-1);
		newcard.setId(1);
		Mockito.when(cardRepository.save(Mockito.any(Card.class))).thenReturn(null);
		int result = cardService.addCard(newcard);
		assertEquals(result,-1);
	}

	@Test
	public void getAllCardsEmpty() {
		List<Card> all = new ArrayList<>();
		Mockito.when(cardRepository.findAll()).thenReturn(all);
		List<Card> result = cardService.getAllCards();
		Mockito.verify(cardRepository).findAll();
		assertEquals(new ArrayList<Card>(),result);
	}
	
	@Test
	public void getAllCards() {
		List<Card> all = new ArrayList<>();
		Mockito.when(cardRepository.findAll()).thenReturn(all);
		
		for(int i=0;i<10;i++) {
			all.add(new Card("superman"+i,"powerfull"+i,"Superman Family"+i,
				"Superman affinity",50,100,550,30,800,"https:\\\\\\\\fakeSite\\\\data"+i+".jpg",-1));
		}
		List<Card> result = cardService.getAllCards();
		Mockito.verify(cardRepository).findAll();
		assertEquals(all,result);
	}
	@Test
	public void getAllCardstoSellEmpty() {
		List<Card> all = new ArrayList<>();
		Mockito.when(cardRepository.findByUserId(Mockito.anyInt())).thenReturn(all);
		List<Card> result = cardService.getAllCardstoSell();
		Mockito.verify(cardRepository).findByUserId(-1);
		assertEquals(new ArrayList<Card>(),result);
	}
	
	@Test
	public void getAllCardstoSell() {
		List<Card> all = new ArrayList<>();
		Mockito.when(cardRepository.findByUserId(Mockito.anyInt())).thenReturn(all);		
		
		for(int i=0;i<10;i++) {
			all.add(new Card("superman"+i,"powerfull"+i,"Superman Family"+i,
				"Superman affinity",50,100,550,30,800,"https:\\\\\\\\fakeSite\\\\data"+i+".jpg",-1));
		}
		List<Card> result = cardService.getAllCardstoSell();
		Mockito.verify(cardRepository).findByUserId(-1);
		assertEquals(all,result);
	}
	
	@Test
	public void getUserCardsEmpty() {
		List<Card> all = new ArrayList<>();
		Mockito.when(cardRepository.findByUserId(Mockito.anyInt())).thenReturn(all);
		List<Card> result = cardService.getUserCards(1);
		Mockito.verify(cardRepository).findByUserId(1);
		assertEquals(new ArrayList<Card>(),result);
	}
	
	@Test
	public void getUserCards() {
		List<Card> all = new ArrayList<>();
		Mockito.when(cardRepository.findByUserId(Mockito.anyInt())).thenReturn(all);		
		for(int i=0;i<10;i++) {
			all.add(new Card("superman"+i,"powerfull"+i,"Superman Family"+i,
				"Superman affinity",50,100,550,30,800,"https:\\\\\\\\fakeSite\\\\data"+i+".jpg",1));
		}
		List<Card> result = cardService.getUserCards(1);
		Mockito.verify(cardRepository).findByUserId(1);
		assertEquals(all,result);
	}

	@Test
	public void getPriceCardSuccess() {
		Mockito.when(cardRepository.findById(Mockito.anyInt())).thenReturn(new Card("superman","powerfull","Superman Family",
				"Superman affinity",50,100,550,30,800,"https:\\\\\\\\fakeSite\\\\data.jpg",-1));
		int price = cardService.getPriceCard(1);
		assertEquals(800,price);
	}
	
	@Test
	public void getPriceCardFail() {
		Mockito.when(cardRepository.findById(Mockito.anyInt())).thenReturn(null);
		int price = cardService.getPriceCard(1);
		assertEquals(-1,price);
	}

	@Test
	public void removeCardFromUserSuccess() {
		Card card = new Card("superman","powerfull","Superman Family",
				"Superman affinity",50,100,550,30,800,"https:\\\\\\\\fakeSite\\\\data.jpg",-1);
		
		Mockito.when(cardRepository.findById(Mockito.anyInt())).thenReturn(card);
		
		Mockito.when(cardRepository.save(Mockito.any(Card.class))).thenReturn(card);
		
		boolean success = cardService.removeCardFromUser(1);
		assertEquals(true, success);
	}
	
	@Test
	public void removeCardFromUserFailGetCard() {
		Mockito.when(cardRepository.findById(Mockito.anyInt())).thenReturn(null);
		
		boolean success = cardService.removeCardFromUser(1);
		assertEquals(false, success);
	}
	
	@Test
	public void removeCardFromUserFailSaveCard() {
		Card card = new Card("superman","powerfull","Superman Family",
				"Superman affinity",50,100,550,30,800,"https:\\\\\\\\fakeSite\\\\data.jpg",-1);
		
		Mockito.when(cardRepository.findById(Mockito.anyInt())).thenReturn(card);
		
		Mockito.when(cardRepository.save(Mockito.any(Card.class))).thenThrow(new NullPointerException());
		
		boolean success = cardService.removeCardFromUser(1);
		assertEquals(false, success);
	}

	@Test
	public void setUserToCardSucess() {
		Card card = new Card("superman","powerfull","Superman Family",
				"Superman affinity",50,100,550,30,800,"https:\\\\\\\\fakeSite\\\\data.jpg",-1);
		
		Mockito.when(cardRepository.findById(Mockito.anyInt())).thenReturn(card);
		
		Mockito.when(cardRepository.save(Mockito.any(Card.class))).thenReturn(card);
		
		boolean success = cardService.setUserToCard(1, 1);
		assertEquals(true, success);
	}
	
	@Test
	public void setUserToCardFailGetCard() {
		Mockito.when(cardRepository.findById(Mockito.anyInt())).thenReturn(null);
		
		boolean success = cardService.setUserToCard(1,1);
		assertEquals(false, success);
	}
	
	@Test
	public void setUserToCardFailSaveCard() {
		Card card = new Card("superman","powerfull","Superman Family",
				"Superman affinity",50,100,550,30,800,"https:\\\\\\\\fakeSite\\\\data.jpg",-1);
		
		Mockito.when(cardRepository.findById(Mockito.anyInt())).thenReturn(card);
		
		Mockito.when(cardRepository.save(Mockito.any(Card.class))).thenThrow(new NullPointerException());
		
		boolean success = cardService.setUserToCard(1,1);
		assertEquals(false, success);
	}
}
