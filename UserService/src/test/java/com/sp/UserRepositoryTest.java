package com.sp;


import static org.junit.Assert.assertTrue;

import java.util.List;

import com.sp.model.User;
import com.sp.repository.UserRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {
    @Autowired
    UserRepository uRepo;

    @Before
    public void setup(){
        System.out.println("[BEFORE TEST] -- Add User to test");
        uRepo.save(new User("jdoe","strong",500));
    }

    @After
    public void cleanUp(){
        System.out.println("[AFTER TEST] -- CLEAN user list");
        uRepo.deleteAll();
    }


    @Test
    public void saveUser(){
        uRepo.save(new User("test","familledetest",1));
        assertTrue(true);
    }

    @Test
    public void findAllTest(){
        List<User> users = uRepo.findAll();
        assertTrue(users.size() == 1);
    }
    
    @Test
    public void findByUsernameAndPasswordTest() {
    	User user = uRepo.findByUsernameAndPassword("jdoe", "strong");
    	assertTrue(user.getUsername().equals("jdoe") && user.getPassword().equals("strong"));
    }

}
