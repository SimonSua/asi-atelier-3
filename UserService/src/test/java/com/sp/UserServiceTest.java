package com.sp;

import com.sp.model.User;
import com.sp.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.sp.service.UserService;
import org.springframework.web.client.RestTemplate;

import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@WebMvcTest(value = UserService.class)
public class UserServiceTest {

    @MockBean
    private RestTemplate restTemplate;

    @MockBean
    private UserRepository uRepo;

    @Autowired
    private UserService uService;


    @Test
    public void getUserExistSuccess() {
        Mockito.when(
                uRepo.findByUsernameAndPassword(Mockito.anyString(), Mockito.anyString())
        ).thenReturn(new User("jdoe", "strong", 100));

        int user_id = uService.getUserExist("jdoe", "strong");
        assertTrue(user_id >=0);
    }

    @Test
    public void getUserExistFail() {
        Mockito.when(
                uRepo.findByUsernameAndPassword(Mockito.anyString(), Mockito.anyString())
        ).thenReturn(null);

        int user_id = uService.getUserExist("jdoe", "strong");
        assertTrue(user_id <0);
    }


    @Test
    public void getUserIdTest() {
        Mockito.when(uRepo.findById(Mockito.anyInt())).thenReturn(new User("test", "test", 100));
        User userInfo=uService.getUser(1);
        assertTrue(userInfo.getUsername().equals("test") && userInfo.getPassword().equals("test"));
    }
}
