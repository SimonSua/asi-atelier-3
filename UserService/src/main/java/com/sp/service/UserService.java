package com.sp.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.List;
import java.util.Optional;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.sp.model.User;
import com.sp.repository.UserRepository;


@Service
public class UserService {

	@Autowired
	UserRepository uRepository;
	
	private final RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
	private final RestTemplate restTemplate = restTemplateBuilder.build();
	private HttpHeaders httpHeaders = new HttpHeaders();

	public User createUser(User user) {
		User createdUser = uRepository.save(user);
		return createdUser;
	}

	public User getUser(int id) {
		User user = uRepository.findById(id);
		return user;

	}

	public int getUserExist(String username, String password){
		try {
			return uRepository.findByUsernameAndPassword(username, password).getId();
		} catch(NullPointerException e) {
			System.out.println("User introuvable");
			return -1;
		}

	}

	public List<User> getAllUsers() {
		List<User> users = new ArrayList<>();
		uRepository.findAll().forEach(users::add);
		return users;
	}

	
	// Service : card
	// Get User Cards
	public List<Integer> getUserCard(int user_id){
		List<Integer> listCardId = new ArrayList<>();
		try {
			 listCardId = this.restTemplate.getForObject("http://localhost/card/userCards/" + user_id, List.class);
		} catch (NullPointerException e) {
			System.out.println(e.getMessage());
		}
		return listCardId;
	}

	// Service : card
	// Create a card
	public boolean createCard(int user_id) {
		boolean answer = false;
		String url = "http://localhost/card/add";

		httpHeaders.setContentType(MediaType.APPLICATION_JSON);
		httpHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		Map<String, Object> parameters = new HashMap<>();
		parameters.put("name", "Carte de base");
		parameters.put("description", "Ceci est une carte de base");
		parameters.put("family", "Skywalker");
		parameters.put("affinity", "LASER");
		parameters.put("hp", 50);
		parameters.put("energy", 5);
		parameters.put("price", 150);
		parameters.put("defence", 10);
		parameters.put("attack", 10);
		parameters.put("imgUrl", "");
		parameters.put("user_id", user_id);
		HttpEntity<Map<String, Object>> entity = new HttpEntity<>(parameters, httpHeaders);
		try {
			answer = this.restTemplate.postForObject(url, entity, boolean.class);

		} catch (NullPointerException e) {
			System.out.println(e.getMessage());
		}
		return answer;
	}
	
	// Service : card
	// Get a card Value
	public int getCardValue(int card_id) {
		//ask card service to know card value /card/price/{idCard}
		int card_value = 0;
		try {
			card_value = this.restTemplate.getForObject("http://localhost/card/price/" + card_id, Integer.class); //Récupère le prix d'une carte par l'id
		} catch (NullPointerException e) {
			System.out.println(e.getMessage());
		}
		
		return card_value;
	}
	
	// Service : card
	// Set a user on a card
	public boolean setCardUser(int user_id, int card_id) {
		boolean answer = false;
		String url = "http://localhost/card/setUser/";
		httpHeaders.setContentType(MediaType.APPLICATION_JSON);
		httpHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		Map<String, Object> parameters = new HashMap<>();
		parameters.put("card_id", card_id);
		parameters.put("user_id", user_id);
		HttpEntity<Map<String, Object>> entity = new HttpEntity<>(parameters, httpHeaders);
		try {
			answer = this.restTemplate.postForObject(url, entity, Boolean.class);
			
		} catch (NullPointerException e) {
			System.out.println(e.getMessage());
		}
		return answer;
	}
	
	// Service : card
	// Remove user on a card
	public boolean removeCardUser(int card_id) {
		boolean answer = false;
		///card/removeUser request body card_id
		String url = "http://localhost/card/removeUser";
		
		try {
			answer = this.restTemplate.postForObject(url, card_id, Boolean.class);
		} catch (NullPointerException e) {
			System.out.println(e.getMessage());
		}
		return answer;
	}

	public boolean buyOrSellCard(String data, boolean toSell){
		boolean answer = false;
		JsonObject jo = new JsonParser().parse(data).getAsJsonObject();
		int card_id = jo.get("card_id").getAsInt();
		int user_id = jo.get("user_id").getAsInt();
		int card_value = getCardValue(card_id);
		System.out.println("---Valeur de la carte : "+card_value + "---");
		User user = uRepository.findById(user_id);
		if(toSell){
			answer = removeCardUser(card_id);
			user.setMoney(user.getMoney() + card_value);
		} else {
			answer = setCardUser(user_id, card_id);
			user.setMoney(user.getMoney() - card_value);
		}
		uRepository.save(user);
		return answer;

	}



	public int getUserMoney(int user_id){
		int userMoney = 0;
		User user = uRepository.findById(user_id);
		userMoney = user.getMoney();
		return userMoney;

	}


}
