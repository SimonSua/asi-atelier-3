package com.sp.rest;

import com.sp.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.sp.model.User;

@RestController
public class UserRestController {

	@Autowired
    UserService uService;

	
	//Service : auth
	//return if user exists
	@RequestMapping(method=RequestMethod.POST, value="/user/userExists")
	public int getUserExist(@RequestBody User user) {
		System.out.println("Endpoint : /user/userExists");
		System.out.println("Find user by ID for id : " + user.getUsername() + " " + user.getPassword());
		int user_id = uService.getUserExist(user.getUsername(), user.getPassword());
		System.out.println("Réponse:");
		System.out.println("userId: " + user_id);
		return user_id;
	}
	
	
	//
	//   MARKET
	//
	
	//Service : market
	//Add money to the bank
	@RequestMapping(method=RequestMethod.POST, value="/user/sellCard")
	public boolean addMoney(@RequestBody String data) {
		System.out.println("----Endpoint : /user/sellCard ----");
		boolean answer = uService.buyOrSellCard(data, true);
		System.out.println("Réponse:");
		System.out.println("Carte Vendue:" + answer);
		return answer;
	}
	
	//Service : market
	//Get money from user ID
	@RequestMapping(method=RequestMethod.GET, value="/user/money/{userId}")
	public int getMoney(@PathVariable String userId) {
		System.out.println("----Endpoint : /user/money/ ----" + userId);
		System.out.println("Get money du user : " + userId);
		int userMoney = uService.getUserMoney(Integer.valueOf(userId));
		System.out.println("Réponse:");
		System.out.println("Argent de User:" + userMoney);
		return userMoney;
	}



	//Service : market
	//add card to User from card id
	@RequestMapping(method=RequestMethod.POST, value="/user/buyCard")
	public boolean addCard(@RequestBody String data) {
		System.out.println("----Endpoint : /user/buyCard ----");
		boolean answer = uService.buyOrSellCard(data, false);
		System.out.println("Réponse:");
		System.out.println("Carte achetée: " + answer);
		
		return answer;
	}

	@RequestMapping(method=RequestMethod.GET, value="/user/info/{userId}")
	public User getUser(@PathVariable String userId){

		System.out.println("----Endpoint : /user/money/ ----" + userId);
		System.out.println("Get info du user : " + userId);
		User user = uService.getUser(Integer.valueOf(userId));
		System.out.println("Réponse:");
		System.out.println("User:" + user.getId());
		return user;
	}


	//Service : Website
	//CreateUser in DB after signUp
	@RequestMapping(method=RequestMethod.POST, value="/user/createUser")
	public boolean createUser(@RequestBody User user){
		System.out.println("----Endpoint : /user/createUser ----");
		User createdUser = uService.createUser(user);
		uService.createCard(createdUser.getId());
		if(createdUser != null) {
			System.out.println(createdUser);
			return true;
		}
		return false;
	}
}
