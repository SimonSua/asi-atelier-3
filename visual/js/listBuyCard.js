var sCookie = document.cookie;
if(sCookie.indexOf("cardapptoken") !== -1) {
    sToken = sCookie.split("cardapptoken=")[1].split(";")[0];
    $.ajax({
        type: "POST",
        url: "/auth/auth",
        data: sToken,
        contentType: "application/json",
        success: function(oUser) {
            if(oUser != -1) {
	    			$.ajax({
			    		type: "GET",
			    		url: "/user/info/"+oUser,
			    		success: function(oUser) {
			    			if(oUser != -1) {
			    				$("#userNameId").text(oUser.username);
			    				$("#cashIcon").after(oUser.money + "$");
			    			}
			    		}
			    	})
    			} else {
    				window.location.href = "connexion.html";
    			}
        }
    })

var last_id = 0;
$(document ).ready(function(){
    $(document).on("click", ".buy", function(){
        info = JSON.stringify({ card_id: last_id,
        token: sToken});
        console.log("Achat"+last_id);
        $.ajax({
            url: "/market/buy",
            data: info,
            contentType: "application/json",
            type: "POST",
            dataType: "json",
            success: function (response) {
                window.location.reload();
            }
          });

    });

	$.ajax({
      url: "/card/tosell",
      type: "GET",
      dataType: "JSON",
	  success : function(code_html, statut){
          console.log($(code_html));
          $.each(code_html, function(i, item){
                addCardToList("", item.family, item.imgUrl, item.name, item.description, item.hp, item.energy, item.attack, item.defence, item.price, item.id);
                fillCurrentCard("",item.family, item.imgUrl, item.name, item.description, item.hp, item.energy, item.attack, item.defence, item.price, item.id);
        });
      }
	}); 
});




function fillCurrentCard(imgUrlFamily,familyName,imgUrl,name,description,hp,energy,attack,defence,price, id){
    //FILL THE CURRENT CARD
    $('#cardFamilyImgId')[0].src=imgUrlFamily;
    $('#cardFamilyNameId')[0].innerText=familyName;
    $('#cardImgId')[0].src=imgUrl;
    $('#cardNameId')[0].innerText=name;
    $('#cardDescriptionId')[0].innerText=description;
    $('#cardHPId')[0].innerText=hp+" HP";
    $('#cardEnergyId')[0].innerText=energy+" Energy";
    $('#cardAttackId')[0].innerText=attack+" Attack";
    $('#cardDefenceId')[0].innerText=defence+" Defence";
    $('#cardPriceId')[0].innerText=price+" $";
    last_id = id;
};


function addCardToList(imgUrlFamily,familyName,imgUrl,name,description,hp,energy,attack,defence,price, id){
    last_id = id;
    content="\
    <td> \
    <img  class='ui avatar image' src='"+imgUrl+"'> <span>"+name+" </span> \
   </td> \
    <td>"+description+"</td> \
    <td>"+familyName+"</td> \
    <td>"+hp+"</td> \
    <td>"+energy+"</td> \
    <td>"+defence+"</td> \
    <td>"+attack+"</td> \
    <td>"+price+"$</td>\
    <td>\
        <div class='ui vertical animated button buy' tabindex='0' >\
            <div class='hidden content'>Buy</div>\
    <div class='visible content'>\
        <i class='shop icon'></i>\
    </div>\
    </div>\
    </td>";
    
    $('#cardListId tbody').append('<tr id="tr'+id+'">'+content+'</tr>');
    $(document).on("click", "#tr"+id, function(){
        fillCurrentCard("",familyName,imgUrl,name,description,hp,energy,attack,defence,price, id);
    });
    
};

} else {
    window.location.href = "connexion.html";
}
