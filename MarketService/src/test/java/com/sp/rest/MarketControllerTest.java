package com.sp.rest;

import com.sp.service.MarketService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@WebMvcTest(value = MarketController.class)

public class MarketControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MarketService marketService;

    @Test
    public void buyCardSuccessTest() throws Exception {
    	Mockito.when(marketService.buyCard(Mockito.anyString())).thenReturn(true);
    	
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/market/buy")
        		.accept(MediaType.APPLICATION_JSON)
        		.content("{\"userd\": \"1\", \"card_id\": \"2\"}");
        
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        assertTrue(result.getResponse().getContentAsString().equals("true"));
    }
    
    @Test
    public void buyCardFailTest() throws Exception {
    	Mockito.when(marketService.buyCard(Mockito.anyString())).thenReturn(false);
    	
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/market/buy")
        		.accept(MediaType.APPLICATION_JSON)
        		.content("{\"userd\": \"1\", \"card_id\": \"2\"}");
        
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        assertTrue(result.getResponse().getContentAsString().equals("false"));
    }
    
    @Test
    public void sellCardSuccessTest() throws Exception {
    	Mockito.when(marketService.sellCard(Mockito.anyString())).thenReturn(true);
    	
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/market/sell")
        		.accept(MediaType.APPLICATION_JSON)
        		.content("{\"userd\": \"1\", \"card_id\": \"2\"}");
        
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        assertTrue(result.getResponse().getContentAsString().equals("true"));
    }
    
    @Test
    public void sellCardFailTest() throws Exception {
    	Mockito.when(marketService.sellCard(Mockito.anyString())).thenReturn(false);
    	
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/market/sell")
        		.accept(MediaType.APPLICATION_JSON)
        		.content("{\"userd\": \"1\", \"card_id\": \"2\"}");
        
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        assertTrue(result.getResponse().getContentAsString().equals("false"));
    }
}
