package com.sp.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@WebMvcTest(value = MarketService.class)
public class MarketServiceTest {

    @Autowired
    private MarketService marketService;

    @MockBean
    private RestTemplate restTemplate;

    String data = "{\"token\": \"1\", \"card_id\": \"2\"}";

    @Test
    public void cantConnectToServiceTest(){
        System.out.println("Testing buyCard function behavior without connection to other services");
        boolean result = marketService.buyCard(data);
        assertTrue(result == false);
    }
    
    @Test
    public void sellCardSuccessTest() {
    	Mockito
        .when(restTemplate.postForObject(Mockito.anyString(), Mockito.any(), Mockito.eq(Boolean.class)))
        .thenReturn(true);
    	
    	Mockito
        .when(restTemplate.postForObject(Mockito.anyString(), Mockito.anyString(), Mockito.eq(Integer.class)))
        .thenReturn(1);
    	
    	boolean answer = marketService.sellCard(data);
    	assertTrue(answer == true);
    }
    
    @Test
    public void sellCardFailAuthTest() {
    	Mockito
        .when(restTemplate.postForObject(Mockito.anyString(), Mockito.anyString(), Mockito.eq(Integer.class)))
        .thenReturn(-1);
    	
    	boolean answer = marketService.sellCard(data);
    	assertTrue(answer == false);
    }
    
    @Test
    public void sellCardFailSellTest() {
    	Mockito
        .when(restTemplate.postForObject(Mockito.anyString(), Mockito.any(), Mockito.eq(Boolean.class)))
        .thenReturn(false);
    	
    	Mockito
        .when(restTemplate.postForObject(Mockito.anyString(), Mockito.anyString(), Mockito.eq(Integer.class)))
        .thenReturn(1);
    	
    	boolean answer = marketService.sellCard(data);
    	assertTrue(answer == false);
    }

    @Test
    public void buyCardSucessTest() {
    	
    	Mockito
        .when(restTemplate.postForObject(Mockito.anyString(), Mockito.anyString(), Mockito.eq(Integer.class)))
        .thenReturn(1);
    	
        Mockito
        .when(restTemplate.getForObject("http://localhost:8080/user/money/" + Mockito.anyString(), Mockito.eq(Integer.class)))
        .thenReturn(200);
        
        Mockito
        .when(restTemplate.getForObject("http://localhost:8080/card/price/" + Mockito.anyString(), Mockito.eq(Integer.class)))
        .thenReturn(200);
        
        Mockito
        .when(restTemplate.postForObject(Mockito.anyString(), Mockito.any(), Mockito.eq(Boolean.class)))
        .thenReturn(true);
        
        boolean result = marketService.buyCard(data);
        assertTrue(result == true);
    }
    
    @Test
    public void buyCardFailAuthTest() {
    	Mockito
        .when(restTemplate.postForObject(Mockito.anyString(), Mockito.anyString(), Mockito.eq(Integer.class)))
        .thenReturn(-1);
        
        boolean result = marketService.buyCard(data);
        assertTrue(result == false);
    }
    
    @Test
    public void buyCardFailNotEnoughMoneyTest() {
    	Mockito
        .when(restTemplate.postForObject(Mockito.anyString(), Mockito.anyString(), Mockito.eq(Integer.class)))
        .thenReturn(1);
    	
        Mockito
        .when(restTemplate.getForObject("http://localhost:8080/user/money/" + Mockito.anyString(), Mockito.eq(Integer.class)))
        .thenReturn(200);
        
        Mockito
        .when(restTemplate.getForObject("http://localhost:8080/card/price/" + Mockito.anyString(), Mockito.eq(Integer.class)))
        .thenReturn(300);
        
        boolean result = marketService.buyCard(data);
        assertTrue(result == false);
    }
    
    @Test
    public void buyCardFailAddCardTest() {
    	Mockito
        .when(restTemplate.postForObject(Mockito.anyString(), Mockito.anyString(), Mockito.eq(Integer.class)))
        .thenReturn(1);
    	
        Mockito
        .when(restTemplate.getForObject("http://localhost:8080/user/money/" + Mockito.anyString(), Mockito.eq(Integer.class)))
        .thenReturn(200);
        
        Mockito
        .when(restTemplate.getForObject("http://localhost:8080/card/price/" + Mockito.anyString(), Mockito.eq(Integer.class)))
        .thenReturn(200);
        
        Mockito
        .when(restTemplate.postForObject(Mockito.anyString(), Mockito.any(), Mockito.eq(Boolean.class)))
        .thenReturn(false);
        
        boolean result = marketService.buyCard(data);
        assertTrue(result == false);
    }

}
