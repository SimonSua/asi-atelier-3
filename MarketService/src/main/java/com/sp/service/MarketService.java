package com.sp.service;

import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Service
public class MarketService {
    @Autowired
    RestTemplate restTemplate;

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    private HttpHeaders httpHeaders = new HttpHeaders();

    public int getUserId(String user_token){
        System.out.println("---- Recupération de l'id via le token ----");
        String url = "http://localhost/auth/auth/";
        httpHeaders.setContentType(MediaType.TEXT_PLAIN);
        httpHeaders.setAccept(Collections.singletonList(MediaType.TEXT_PLAIN));

        try {
            return restTemplate.postForObject(url, user_token, Integer.class); //Requête Post pour l'ajout de la carte à UserService
        }catch (NullPointerException e){
            System.out.println(e.getMessage() + " User introuvable");
            return -1;
        }
    }

    public boolean sellCard(String data){
        //Le code suivant est présent dans les fonctions sellCard et buyCard -> to do : fonction pour factoriser ce code
        boolean answer = false;
        JsonObject jo = new JsonParser().parse(data).getAsJsonObject();

        String user_token = null;
        int user_id = -1;
        int card_id = -1;

        try {
            user_token = jo.get("token").getAsString();
            card_id = jo.get("card_id").getAsInt();
        }catch (JsonIOException e){
            System.out.println("Problème de données dans le JSON");
        }

        //On récupère l'id de l'user via le token en le vérifiant avec le AuthService
        user_id = this.getUserId(user_token);
        if(user_id == -1) {
        	return false;
        }
        //System.out.println("user id : "+user_id+" et card id : "+card_id);

        String url = "http://localhost/user/sellCard/"; //Url pour dire à UserService d'ajouter une carte selon l'id dans le post

        //On prépare le headers de la requête
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        Map<String, Object> parameters = new HashMap<>(); //On prépare le body du post
        parameters = new HashMap<>(); //On prépare le body du post
        parameters.put("user_id", user_id);
        parameters.put("card_id", card_id);
        HttpEntity<Map<String, Object>> entity = new HttpEntity<>(parameters, httpHeaders);

        try {
            answer = this.restTemplate.postForObject(url, entity, Boolean.class); //Requête Post pour l'ajout de la carte à UserService
        }catch (NullPointerException e){
            System.out.println(e);
        }
        return answer;
    }

    public boolean buyCard(String data){
        boolean answer = false;
        JsonObject jo = new JsonParser().parse(data).getAsJsonObject();
        String user_token = null;
        int user_id = -1;
        int card_id = -1;
        try {
            user_token = jo.get("token").getAsString();
            card_id = jo.get("card_id").getAsInt();
        }catch (JsonIOException e){
            System.out.println("Problème de données dans le JSON");
        }
        //On récupère l'id de l'user via le token en le vérifiant avec le AuthService
        user_id = this.getUserId(user_token);
        if(user_id == -1) {
        	return false;
        }

        //System.out.println("user id : "+user_id+" et card id : "+card_id);
        //Demande à User Service si l'user possède le pécule nécessaire
        int user_money = 0; //Valeur si les requêtes ne répondent pas
        int card_price = 1; //Valeur si les requêtes ne répondent pas, 1 pour que l'User sans argent ne puissent pas acheter une carte en cas d'erreur
        try {
            user_money = restTemplate.getForObject("http://localhost/user/money/" + user_id, Integer.class); //On récupère l'argent de l'user via UserService
            card_price = restTemplate.getForObject("http://localhost/card/price/" + card_id, Integer.class); //On récupère le prix de la carte via CardsService
        }catch (NullPointerException e){
            System.out.println("Pas de valeur");
            System.out.println(e.getMessage()); //En cas d'erreur
            return false;
        }catch (Exception e){
            System.out.println("Impossible de se connecter aux autres services");
            System.out.println(e.getCause());
            return false;
        }
        if(user_money >= card_price){ //On vérifie si le pécule est suffisant
            String url = "http://localhost/user/buyCard/"; //Url pour dire à UserService d'ajouter une carte selon l'id dans le post
            //On prépare le headers de la requête
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

            Map<String, Object> parameters = new HashMap<>(); //On prépare le body du post
            parameters.put("user_id", user_id);
            parameters.put("card_id", card_id);
            HttpEntity<Map<String, Object>> entity = new HttpEntity<>(parameters, httpHeaders);
            try {
                System.out.println("On demande à UserService d'ajouter la carte");
                answer = restTemplate.postForObject(url, entity, Boolean.class); //Requête Post pour l'ajout de la carte à UserService
            }catch (Exception e){
                System.out.println(e);
                return false;
            }
        }else{
            System.out.println("L'user numéro "+user_id+ "n'a pas assez de pécule pour acheter la carte numéro " +card_id);
            answer = false;
        }
        return answer;
    }
}
