package com.sp.rest;


import com.sp.service.MarketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MarketController {
    @Autowired
    MarketService marketService;

    @RequestMapping(method = RequestMethod.POST, value="/market/sell")
    public boolean sellCard(@RequestBody String data){
        return marketService.sellCard(data);
    }

    @RequestMapping(method = RequestMethod.POST, value="/market/buy")
    public boolean buyCard(@RequestBody String data){
        return marketService.buyCard(data);
    }
}
