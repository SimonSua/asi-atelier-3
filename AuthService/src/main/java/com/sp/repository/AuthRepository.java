package com.sp.repository;

import org.springframework.data.repository.CrudRepository;

import com.sp.model.Auth;

public interface AuthRepository extends CrudRepository<Auth, Integer> {
	public Auth findByToken(String token);
	public Auth findByUserId(int userId);
}
