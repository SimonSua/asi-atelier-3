package com.sp.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sp.model.UserLogin;
import com.sp.service.AuthService;


@RestController
public class AuthRestController {
	
	@Autowired
    AuthService aService;
	
	// Methode permettant de verifier l'identite d'un utilisateur grace a son token
	// Renvoi le userId
	@RequestMapping(method=RequestMethod.POST, value="/auth/auth")
	public int auth(@RequestBody String token) {
		System.out.println();
		System.out.println("----Vérification token----");
		System.out.println("Token: " + token);
		int userId = aService.checkToken(token);
		System.out.println("Réponse:");
		System.out.println("userId: " + userId);
		return userId;
	}
	
	// Methode permettant de se connecter grace au username et au password
	// Renvoi le token généré
	@RequestMapping(method=RequestMethod.POST, value="/auth/login")
	public String login(@RequestBody UserLogin login) {
		System.out.println();
		System.out.println("----Connexion----");
		System.out.println(login.toJSONString());
		String token = aService.login(login);
		System.out.println("Réponse:");
		System.out.println("token: " + token);
		return token;
	}
}
