package com.sp.service;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.sp.model.Auth;
import com.sp.model.UserLogin;
import com.sp.repository.AuthRepository;

@Service
public class AuthService {
	
	@Autowired
	AuthRepository aRepository;
	
	@Autowired
    RestTemplate restTemplate;
	
	@Bean
	public RestTemplate restTemplate() {
	    return new RestTemplate();
	}

	private HttpHeaders httpHeaders = new HttpHeaders();

	// Cherche dans la base de donnée le user correspondant au token recu
	// Renvoie le userId correspondant ou -1
	public int checkToken(String token) {
		try {
			return aRepository.findByToken(token).getUserId();
		} catch(NullPointerException e) {
			System.out.println("Token introuvable");
			return -1;
		}
	}
	
	// Demande au service user le user correspondant aux informations recues
	// Renvoie le token généré s'il le trouve
	// Renvoie un String vide sinon
	public String login(UserLogin login) {

		String url = "http://localhost/user/userExists/";
		int userId = -1;
		httpHeaders.setContentType(MediaType.APPLICATION_JSON);
		httpHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

		Map<String, Object> parameters = new HashMap<>();
		parameters.put("username", login.getUsername());
		parameters.put("password", login.getPassword());
		HttpEntity<Map<String, Object>> entity = new HttpEntity<>(parameters, httpHeaders);
		try {
			userId = restTemplate.postForObject(url, entity, Integer.class);
		}catch (NullPointerException e){
			System.out.println("Connexion impossible avec le service User");
		}

		if(userId != -1) {
			String token = generateToken(login);
			Auth user = new Auth(token, userId);
			try {
				aRepository.save(user);
				System.out.println("Connexion user id: " + userId + " token: "+ token);
				return token;
			} catch(Exception e) {
				System.out.println("Sauvegarde du token impossible");
			}
		} else {
			System.out.println("Information du User eronnée");
		}
		return "";
	}
	
	// Genere le token depuis les informations del'utilisateur
	private String generateToken(UserLogin login) {
		return login.getPassword() + login.getUsername() + new Date().getTime();
	}
	
}
