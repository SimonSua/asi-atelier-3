package com.sp.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Auth {
	@Id
	private int userId;
	private String token;
	
	public Auth(String token, int userId) {
		this.token = token;
		this.userId = userId;
	}
	
	public Auth() {}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	@Override
	public String toString() {
		return "User: " + userId + "; token: " + token;
	}
}
