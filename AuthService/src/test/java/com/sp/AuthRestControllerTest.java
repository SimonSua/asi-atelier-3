package com.sp;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.sp.model.UserLogin;
import com.sp.rest.AuthRestController;
import com.sp.service.AuthService;

@RunWith(SpringRunner.class)
@WebMvcTest(value = AuthRestController.class)
public class AuthRestControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private AuthService aService;

	@Test
	public void authSucessTest() throws Exception {
		Mockito.when(aService.checkToken(Mockito.anyString())).thenReturn(0);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/auth/auth")
				.content("alexalex")
				.accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		assertTrue(result.getResponse().getContentAsString().equals("0"));
	}
	
	@Test
	public void authFailTest() throws Exception {
		Mockito.when(aService.checkToken(Mockito.anyString())).thenReturn(-1);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/auth/auth")
				.content("alexalex")
				.accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		assertTrue(result.getResponse().getContentAsString().equals("-1"));
	}
	
	@Test
	public void loginSucessTest() throws Exception {
		Mockito.when(aService.login(Mockito.any())).thenReturn("jeSuisLeToken");
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/auth/login")
				.content(new UserLogin("username", "password").toJSONString())
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		assertTrue(result.getResponse().getContentAsString().equals("jeSuisLeToken"));
	}
	
	@Test
	public void loginFailTest() throws Exception {
		Mockito.when(aService.login(Mockito.any())).thenReturn("");
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/auth/login")
				.content(new UserLogin("username", "password").toJSONString())
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		assertTrue(result.getResponse().getContentAsString().equals(""));
	}
}
