package com.sp;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.sp.model.Auth;
import com.sp.repository.AuthRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class AuthRepositoryTest {
	
	@Autowired
	private AuthRepository aReposiroty;
	
	@Before
    public void setUp() {
		aReposiroty.save(new Auth("alexalex", 0));
    }

    @After
    public void cleanUp() {
    	aReposiroty.deleteAll();
    }
    
    @Test
    public void saveTest() {
    	aReposiroty.save(new Auth("jeSuisLeTest", 1));
    	assertTrue(true);
    }
    
    @Test
    public void findByTokenTest() {
    	Auth user = aReposiroty.findByToken("alexalex");
    	assertTrue(user.getUserId() == 0);
    }
    
    @Test
    public void findByUserIdTest() {
    	Auth user = aReposiroty.findByUserId(0);
    	assertTrue(user.getToken().equals("alexalex"));
    }
}
