package com.sp;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import com.sp.model.Auth;
import com.sp.model.UserLogin;
import com.sp.repository.AuthRepository;
import com.sp.service.AuthService;

@RunWith(SpringRunner.class)
@WebMvcTest(value = AuthService.class)
public class AuthServiceTest {
	
	@MockBean
    private RestTemplate restTemplate;
	
	@MockBean
	private AuthRepository aRepository;
	
	@Autowired
    private AuthService authService;
	
	@Test
    public void cantConnectToServiceTest(){
        String token= authService.login(new UserLogin("alex","alex"));
        assertTrue(token.equals(""));
    }
	
	@Test
	public void loginSuccessTest() {
		Mockito
        .when(restTemplate.postForObject(Mockito.anyString(), Mockito.any(), Mockito.eq(Integer.class)))
        .thenReturn(1);
		
		String token = authService.login(new UserLogin("alex","alex"));
		assertTrue(token.substring(0, 8).equals("alexalex"));
	}
	
	@Test
	public void loginFailTest() {
		Mockito
		.when(restTemplate.postForObject(Mockito.anyString(), Mockito.any(), Mockito.eq(Integer.class)))
        .thenReturn(-1);
		
		String token = authService.login(new UserLogin("alex","alex"));
		assertTrue(token.equals(""));
	}
	
	@Test
	public void checkTokenSuccessTest() {
		Mockito
        .when(aRepository.findByToken(Mockito.anyString()))
        .thenReturn(new Auth("alexalex", 1));
		
		int userId = authService.checkToken("alexalex");
		assertTrue(userId == 1);
	}
	
	@Test
	public void checkTokenFailTest() {
		Mockito
        .when(aRepository.findByToken(Mockito.anyString()))
        .thenReturn(null);
		
		int userId = authService.checkToken("alexalex");
		assertTrue(userId == -1);
	}
}
