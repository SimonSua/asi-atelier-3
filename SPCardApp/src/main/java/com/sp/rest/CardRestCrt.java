 package com.sp.rest;

  import org.springframework.beans.factory.annotation.Autowired;
  import org.springframework.web.bind.annotation.*;

  import com.sp.model.Card;
import com.sp.model.User;
import com.sp.repository.CardRepository;
import com.sp.repository.UserRepository;
import com.sp.service.CardService;
import com.sp.service.UserService;

  import java.util.List;

 @RestController
  public class CardRestCrt {
      @Autowired
      CardService cService;
      @Autowired
      UserService uService;

      @RequestMapping(method=RequestMethod.POST,value="/card")
      public void addCard(@RequestBody Card card) {
          cService.addCard(card);
      }
      
      @RequestMapping(method=RequestMethod.GET,value="/card/{id}")
      public Card getCard(@PathVariable String id) {
          Card c=cService.getCard(Integer.valueOf(id));
          return c;
      }

      @RequestMapping(method=RequestMethod.GET,value="/cards")
      public List<Card> getAllCards(){
          return cService.getAllCards();
      }

      @RequestMapping(method=RequestMethod.POST, value="/card/sellCard/{idCard}")
      private void sellCard(@PathVariable Integer idCard, @RequestBody String token) {
          User user = uService.getUserByToken(token);
          Card c = cService.getCard(idCard);
          user.removeCard(c);
          c.setUser(null);
          uService.updateUser(user);
          cService.updateCard(c);
      }

      @RequestMapping(method=RequestMethod.GET,value="/cardsForSale")
      public List<Card> getAllForSaleCards(){
         return cService.getAllForSaleCards();
     }
  }
