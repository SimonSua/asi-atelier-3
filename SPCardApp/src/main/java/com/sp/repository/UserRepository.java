package com.sp.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.sp.model.Card;
import com.sp.model.User;

public interface UserRepository extends CrudRepository<User, Integer> {
	
	public User findByUsernameAndPassword(String username, String password);
	public List<User> findAll();
	public User findByToken(String token);

}
